(function($) {
    ;(function($, window, document, undefined) {
        $.fn.doubleTapToGo = function(params) {
            if (!('ontouchstart' in window) &&
                !navigator.msMaxTouchPoints &&
                !navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) return false;

            this.each(function() {
                var curItem = false;

                $(this).on('click', function(e) {
                    var item = $(this);
                    if (item[0] != curItem[0]) {
                        e.preventDefault();
                        curItem = item;
                    }
                });

                $(document).on('click touchstart MSPointerDown', function(e) {
                    var resetItem = true,
                        parents = $(e.target).parents();

                    for (var i = 0; i < parents.length; i++)
                        if (parents[i] == curItem[0])
                            resetItem = false;

                    if (resetItem)
                        curItem = false;
                });
            });
            return this;
        };
    })(jQuery, window, document);
    $(document).ready(function() {
        $('.main-menu li:has(ul)').doubleTapToGo();
        // $( ".les-member-section" ).wrapInner( "<div class='member'></div>" );

        $('.slide-section').owlCarousel({

            navigation: false, // Show next and prev buttons
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            autoPlay: true,

        });
        $('.single-slider .next-slide-arrow').on('click', function() {
            var el = $(this);
            $('.single-slider .prev-slide-arrow').removeClass('hidden');
            $('.single-slider img.active').next().addClass('active').siblings('img').removeClass('active');
            if ($('.single-slider img:last-of-type').hasClass('active')) {
                el.addClass('hidden');
            }
        });
        $('.single-slider .prev-slide-arrow').on('click', function() {
            var el = $(this);
            $('.single-slider .next-slide-arrow').removeClass('hidden');
            $('.single-slider img.active').prev().addClass('active').siblings('img').removeClass('active');
            if ($('.single-slider img:first-of-type').hasClass('active')) {
                el.addClass('hidden');
            }
        });
        if ($('.single-slider img:first-of-type').hasClass('active')) {
            $('.single-slider .prev-slide-arrow').addClass('hidden');
        }
        /*$('.single-slider').owlCarousel({

              navigation : true, // Show next and prev buttons
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem:true,
              autoHeight : true,
              autoHeightClass: 'owl-height'
        });*/
        $('.profile-img').on('mouseover', function() {
            $(this).parents('.les-member-section').addClass('hover');
        });
        $('.profile-img').on('mouseleave', function() {
            $(this).parents('.les-member-section').removeClass('hover');
        });
        $('.menu-btn').on('click', function(e) {
            e.preventDefault();
            $('.menu-wrapper').addClass('slide-in');
        });
        $('.back-btn').on('click', function(e) {
            e.preventDefault();
            $('.menu-wrapper').removeClass('slide-in');
        });
        $('.menu-wrapper, .menu-btn').on('click', function(e) {
            e.stopPropagation();
        });
        $(document).on('click', function() {
            $('.menu-wrapper').removeClass('slide-in');
        });
        $('.login-btn, .register-btn').on('click', function(e) {
            e.preventDefault();
            var target = $('#login-popup');
            $(target).fadeIn();
            $('body, html').addClass('ov_hidden');
            console.log(target);
        });
        $('.cross-btn').on('click', function(e) {
            e.preventDefault();
            $(this).closest('.popup-overlay').fadeOut();
            $('body, html').removeClass('ov_hidden');
        });
        $(document).on('submit', "#contactForm", function(event) {
            event.preventDefault();

        //     var url = $(this).attr('action');
        //     var data = $(this).serialize();
        //     var valid = 1;
        //     var email = $('#email');
        //     var name = $('#name');
        //     var prename = $('#prename');
        //     var number = $('#phone');
        //     var message = $('#message');
        //     var cat = $('#cat-select');
        //     var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        //     if (name.val() === "") {
        //         valid = 0;
        //         name.addClass('error');
        //     } else {
        //         name.removeClass('error');
        //     }
        //     if (prename.val() === "") {
        //         valid = 0;
        //         prename.addClass('error');
        //     } else {
        //         prename.removeClass('error');
        //     }
        //     if (number.val() === "") {
        //         valid = 0;
        //         number.addClass('error');
        //     } else {
        //         number.removeClass('error');
        //     }
        //     if (cat.val() === "") {
        //         valid = 0;
        //         cat.addClass('error');
        //     } else {
        //         cat.removeClass('error');
        //     }

        //     if (message.val() === "") {
        //         valid = 0;
        //         message.addClass('error');
        //     } else {
        //         message.removeClass('error');
        //     }

        //     if (email.val() === "") {
        //         valid = 0;
        //         email.addClass('error');
        //     } else {
        //         email.removeClass('error');
        //     }

        //     if (!filter.test(email.val())) {
        //         valid = 0;
        //         email.addClass('error');
        //     } else {
        //         email.removeClass('error');
        //     }



        //     if (valid == 1) {
        //         console.log('submit');
                $('#contact-popup').fadeIn();
                $('body, html').addClass('ov_hidden');
            // }
        });
        if ($('.tp-banner').length > 0) {
            jQuery('.tp-banner').revolution({
                delay: 9000,
                sliderLayout: "auto",
                startwidth: 1170,
                startheight: 272,
                hideThumbs: 10,
                sliderType: "standard"
            });
        }
        $('.main-menu li').each(function() {
            if ($(this).children('ul').length > 0) {
                $(this).addClass('has-children');
            }
        });
        $('.main-menu li.has-children a').on('click', function(e) {
            console.log($(this).html());
            if ((!$(this).hasClass('active')) && ($(window).width() <= 991)) {
                e.preventDefault();
                $(this).next().slideDown();
                $(this).addClass('active');
            } else if(($(this).hasClass('active')) && ($(window).width() >= 992)){
                $(this).next().slideUp();
                $(this).removeClass('active');
            }

        });
    });
    $(window).load(function() {
        $('.single-slider img').each(function() {
            // console.log($(this).height() + $(this).attr('src') + ' height');
            // console.log($(this).width() + $(this).attr('src') + ' width');
            $(this).attr('data-height', $(this).height());
            $(this).attr('data-width', $(this).width());
        });
    });
})(jQuery);
